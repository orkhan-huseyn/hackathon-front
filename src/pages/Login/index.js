import React from 'react';
import Helmet from 'react-helmet';
import LoginForm from '../../components/LoginForm';

export default class Login extends React.Component {

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>Daxil ol</title>
        </Helmet>
        <LoginForm/>
      </React.Fragment>
    );
  }

}