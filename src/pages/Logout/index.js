import React from 'react';
import { Redirect } from 'react-router-dom';
import { TOKEN_KEY, USER_DISPLAY_NAME_KEY, USER_ID_KEY } from '../../api/utils';

class Logout extends React.Component {

  componentDidMount() {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.removeItem(USER_ID_KEY);
    localStorage.removeItem(USER_DISPLAY_NAME_KEY);
  }

  render() {
    return <Redirect to="/login"/>
  }

}

export default Logout;