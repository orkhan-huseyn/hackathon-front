import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../../components/Layout';
import withAuth from "../../hoc/withAuth";
import DirectPaymentList from "../../components/DirectPayment/List";

class DirectPayment extends React.Component {

  render() {

    return (
      <Layout>
        <Helmet>
          <title>Birbaşa ödə</title>
        </Helmet>
        <DirectPaymentList/>
      </Layout>
    );

  }

}

export default withAuth(DirectPayment);