import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../../components/Layout';
import PlanList from '../../components/PaymentPlans/List';
import NewPlan from "../../components/PaymentPlans/NewPaymentPlan";
import withAuth from "../../hoc/withAuth";

class PaymentPlans extends React.Component {

  render() {

    return (
      <Layout>
        <Helmet>
          <title>Ödənişlər planı</title>
        </Helmet>
        <NewPlan/>
        <PlanList/>
      </Layout>
    );

  }

}

export default withAuth(PaymentPlans);