import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../../components/Layout';
import DashboardComponent from "../../components/Dashboard";
import withAuth from "../../hoc/withAuth";

class Dashboard extends React.Component {
  render() {
    return (
      <Layout>
        <Helmet>
          <title>Lövhə</title>
        </Helmet>
        <DashboardComponent/>
      </Layout>
    );
  }
}

export default withAuth(Dashboard);