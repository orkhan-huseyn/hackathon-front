import React from 'react';
import Loadable from 'react-loadable';
import Loading from '../../components/Loading';

const LoadableDashboard = Loadable({
  loader: () => import('./index'),
  loading: Loading,
});

export default () => {
  return <LoadableDashboard/>;
};