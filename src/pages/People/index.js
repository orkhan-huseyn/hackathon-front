import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../../components/Layout';
import PeopleList from '../../components/People/PeopleList'
import NewPeople from '../../components/People/NewPeople'
import withAuth from "../../hoc/withAuth";

class People extends React.Component {

  render() {
    return (
      <Layout>
        <Helmet>
          <title>Şəxslər</title>
        </Helmet>
        <NewPeople/>
        <PeopleList/>
      </Layout>
    );
  }
}

export default withAuth(People);