import axios from 'axios';

export const TOKEN_KEY = 'api_token';
export const USER_DISPLAY_NAME_KEY = 'usr_dsply_name';
export const USER_ID_KEY = 'usr_id';

const defaultHeaders = {
  'Content-Type': 'application/json',
  'IpAddress': '192.168.1.1'
};

if (localStorage.getItem(TOKEN_KEY)) {
  defaultHeaders['Authorization'] = 'Bearer ' + localStorage.getItem('api_token');
}

export const service = axios.create({
  baseURL: 'http://localhost:8080/v1/',
  headers: {
    ...defaultHeaders
  }
});