import React from 'react';
import { Icon } from 'antd';
import { NavLink } from 'react-router-dom';

export default function NotFound() {

  return (
    <div className="arrange-center text-center">
      <h2>Səhifə tapılmadı :( </h2>
      <NavLink to="/">
        Əsas səhifəyə qayıt &nbsp;
        <Icon type="arrow-right"/>
      </NavLink>
    </div>
  );

}