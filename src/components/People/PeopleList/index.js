import React from 'react'
import { Table } from 'antd'

export default class People extends React.Component {
  render() {
    const dataSource = [{
      key: '1',
      name: 'Mike',
      cardNo: 7891234567123456,
      iban: 87412147896325
    }, {
      key: '2',
      name: 'John',
      cardNo: 1234567891234567,
      iban: 14789632587412
    }];

    const columns = [{
      title: 'Adı',
      dataIndex: 'name',
      key: 'name',
    }, {
      title: 'Kart nömrəsi',
      dataIndex: 'cardNo',
      key: 'cardNo'
    }, {
      title: 'Hesab nömrəsi',
      dataIndex: 'iban',
      key: 'iban',
    }];

    return(
      <React.Fragment>
        <h1 className="text-gradient">Şəxslər</h1>
        <Table dataSource={dataSource} columns={columns} />
      </React.Fragment>
    )
  }
}