import React from 'react'
import { Form, Input, InputNumber, Tooltip, Icon, Col, Button } from 'antd';

const FormItem = Form.Item;

class PeopleForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Col span={24}>
          <FormItem
            label="Adı"
          >
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Zəhmət olmasa adı daxil edin', whitespace: true }],
            })(
              <Input/>
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem
            label={(
              <span>
              Hesab nömrəsi&nbsp;
                <Tooltip title="Hesab nömrəsi">
                <Icon type="question-circle-o"/>
              </Tooltip>
            </span>
            )}
          >
            {getFieldDecorator('accountNo', {
              rules: [{ required: true, message: 'Zəhmət olmasa hesab nömrəsini daxil edin', whitespace: true }],
            })(
              <InputNumber style={{ width: '100%' }}/>
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem
            label={(
              <span>
              Bank nömrəsi&nbsp;
                <Tooltip title="Bank nömrəsi">
                <Icon type="question-circle-o"/>
              </Tooltip>
            </span>
            )}
          >
            {getFieldDecorator('iban', {
              rules: [{ required: true, message: 'Zəhmət olmasa bank nömrəsini daxil edin', whitespace: true }],
            })(
              <InputNumber style={{ width: '100%' }}/>
            )}
          </FormItem>
        </Col>
        <Col span={24}>
          <FormItem
            label={(
              <span>
              Nəsə&nbsp;
                <Tooltip title="What do you want others to call you?">
                <Icon type="question-circle-o"/>
              </Tooltip>
            </span>
            )}
          >
            {getFieldDecorator('something', {
              rules: [{ required: true, message: 'Zəhmət olmasa adı daxil edin', whitespace: true }],
            })(
              <Input/>
            )}
          </FormItem>
        </Col>
        <Col md={{ span: 6, offset: 18 }}>
          <FormItem>
            <Button type="primary" htmlType="submit" style={{ width: '100%' }}>Təsdiqlə</Button>
          </FormItem>
        </Col>
      </Form>
    );
  }
}

const NewPeopleForm = Form.create()(PeopleForm);
export default NewPeopleForm