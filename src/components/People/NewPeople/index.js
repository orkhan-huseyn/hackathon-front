import React from 'react'
import { Button, Drawer, } from 'antd'
import PeopleForm from "./Form";

export default class NewPeople extends React.Component {
  state = {
    drawerOpen: false
  };

  toggleDrawer = () => {
    this.setState({
      drawerOpen: !this.state.drawerOpen
    })
  };

  render(){
    return(
      <React.Fragment>
        <Button
          type="primary"
          icon="plus"
          onClick={this.toggleDrawer}
          className="float-right"
          style={{marginBottom: 24}}
        >
          Əlavə et
        </Button>
        <Drawer
          title="Yeni şəxs"
          width={450}
          placement="right"
          onClose={this.toggleDrawer}
          visible={this.state.drawerOpen}
          style={{
            height: 'calc(100% - 55px)',
            overflow: 'auto',
            paddingBottom: 53,
          }}
        >
          <PeopleForm/>
        </Drawer>
      </React.Fragment>
    )
  }
}