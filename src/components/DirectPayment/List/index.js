import React from 'react';
import { Table } from 'antd';

export default class DirectPaymentList extends React.Component {
  render() {
    const dataSource = [{
      key: '1',
      name: 'Mike',
      age: 32,
      address: '10 Downing Street'
    }, {
      key: '2',
      name: 'John',
      age: 42,
      address: '10 Downing Street'
    }];

    const columns = [{
      title: 'Adı',
      dataIndex: 'name',
      key: 'name',
    }, {
      title: 'Məbləğ',
      dataIndex: 'age',
      key: 'age',
    }, {
      title: 'Nəsə',
      dataIndex: 'address',
      key: 'address',
    }];

    return (
      <React.Fragment>
        <div>
          <h1 className="text-gradient">Son əməliyyatlar</h1>
          <Table dataSource={dataSource} columns={columns} pagination={false}/>
        </div>
      </React.Fragment>
    );
  }
}
