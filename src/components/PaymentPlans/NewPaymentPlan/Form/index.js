import React from 'react';
import { Form, Button, Col, Row, Select, Icon, message } from 'antd';
import Expense from "../Expense";
import { service } from "../../../../api/utils";

const { Option } = Select;

class ExpenseForm extends React.Component {

  state = {
    expenseTypes: [
      { text: 'Köçürmə', value: 'transfer', icon: 'retweet' },
      { text: 'Mobil rabitə', value: 'mobileConnection', icon: 'mobile' },
      { text: 'İnternet', value: 'internet', icon: 'global' },
      { text: 'TV', value: 'tv', icon: 'desktop' },
      { text: 'Kommunal xidmətlər', value: 'utilityServices', icon: 'home' },
    ],
    selectedExpenseType: ''
  };

  onExpenseTypeSelected = (val) => {
    this.setState({
      selectedExpenseType: val
    })
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        service.post('templates', values)
          .then(res => {
            message.success('Bazaya əlavə olundu')
          })
          .catch(error => {
            message.error(error.message)
          })
      }
    });
  };

  render() {

    const { getFieldDecorator } = this.props.form;

    return (
      <React.Fragment>
        <Form layout="vertical">
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="Ödənişin növü">
                {getFieldDecorator('paymentType', {
                  rules: [{ required: true, message: 'Ödənişin növü seçilməyib!' }],
                })(
                  <Select placeholder="Ödənişin növünü seçin" onChange={this.onExpenseTypeSelected}>
                    {this.state.expenseTypes.map(type => (
                      <Option key={type.value}>
                        <Icon type={type.icon}/>
                        &nbsp;{type.text}
                      </Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
            </Col>
          </Row>
          {/* Expenses will be loaded dynamically by type */}
          <Expense type={this.state.selectedExpenseType} form={this.props.form}/>
        </Form>
        <div
          style={{
            position: 'absolute',
            bottom: 0,
            width: '100%',
            borderTop: '1px solid #e8e8e8',
            padding: '10px 16px',
            textAlign: 'right',
            left: 0,
            background: '#fff',
            borderRadius: '0 0 4px 4px',
          }}
        >
          <Button
            style={{ marginRight: 8 }}
            onClick={this.props.onClose}
          >
            İmtina
          </Button>
          <Button onClick={this.onSubmit} type="primary">Təsdiq</Button>
        </div>
      </React.Fragment>
    );
  }
}

const WrappedExpenseForm = Form.create()(ExpenseForm);

export default WrappedExpenseForm;