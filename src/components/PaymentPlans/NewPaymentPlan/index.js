import React from 'react';
import { Button, Drawer } from 'antd';
import ExpenseForm from './Form';

export default class NewExpense extends React.Component {

  state = {
    drawerOpen: false
  };

  showDrawer = () => {
    this.setState({
      drawerOpen: true,
    });
  };

  onClose = () => {
    this.setState({
      drawerOpen: false,
    });
  };

  render() {
    return (
      <React.Fragment>
        <Button
          type="primary"
          icon="plus"
          onClick={this.showDrawer}
          style={{ left: '90%' }}
        >
          Əlavə et
        </Button>
        <Drawer
          title="Yeni ödəniş planı"
          width={720}
          placement="right"
          onClose={this.onClose}
          visible={this.state.drawerOpen}
          style={{
            height: 'calc(100% - 55px)',
            overflow: 'auto',
            paddingBottom: 53,
          }}
        >
          <ExpenseForm onClose={this.onClose}/>
        </Drawer>
      </React.Fragment>
    );
  }

}