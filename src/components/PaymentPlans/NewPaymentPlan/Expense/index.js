import React from 'react';
import PropTypes from 'prop-types';

import TransferExpense from './Transfer';
import MobileCommunicationExpense from './MobileCommunication';

const map = {
  transfer: TransferExpense,
  mobileConnection: MobileCommunicationExpense
};

class Expense extends React.Component {

  render() {

    const { type, form } = this.props;

    if (!type || !map[type]) {
      return null;
    }

    const ChosenExpense = map[type];

    return <ChosenExpense form={form}/>;
  }

}

Expense.propTypes = {
  type: PropTypes.string.isRequired,
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func
  }).isRequired
};

export default Expense;