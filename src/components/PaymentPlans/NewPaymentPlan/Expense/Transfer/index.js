import React from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
  Form,
  Input,
  DatePicker,
  Checkbox,
  Tooltip,
  Icon,
  Select
} from 'antd';
import { service } from "../../../../../api/utils";

const { Option } = Select;

class TransferExpense extends React.Component {

  state = {
    receiverList: [],
    periodList: [],
    isPeriodic: false
  };

  componentDidMount() {
    this.getPeriodList();
    this.getRecepientsList()
  }

  getPeriodList = () => {
    service.get('periods')
      .then(response => {
        this.setState({ loading: false });
        if (response.data) {
          this.setState({periodList: response.data})
        }
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  };

  getRecepientsList = () => {
    service.get('recepients')
      .then(response => {
        this.setState({ loading: false });
        if (response.data) {
          this.setState({receiverList: response.data})
        }
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  };

  onPeriodicChanged = (e) => {
    this.setState({
      isPeriodic: e.target.checked
    });
  };

  render() {

    const { getFieldDecorator } = this.props.form;

    return (
      <React.Fragment>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="Ödənişin adı">
              {
                getFieldDecorator('title', {
                  rules: [{ required: true, message: 'Ödənişin adı daxil edilməyib!' }]
                })(
                  <Input placeholder="Ödənişin adını daxil edin..."/>
                )
              }
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Ödəniş ediləcək şəxs">
              {
                getFieldDecorator('receiver', {
                  rules: [{ required: false }]
                })(
                  <Select placeholder="Ödəniş ediləcək şəxsi seçin...">
                    {this.state.receiverList.map(receiver => (
                      <Option key={receiver.value} title={receiver.text}>
                        <span>{receiver.text}</span>{" "}
                      </Option>
                    ))}
                  </Select>
                )
              }
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item label={
              <span>
                Məbləğ (AZN) &nbsp;
                <Tooltip title="Ödənişin valyutası sizin hazırki valyuta seçiminizə görə müəyyən edilir">
                <Icon type="question-circle-o"/>
              </Tooltip>
            </span>
            }>
              {
                getFieldDecorator('amount', {
                  rules: [{ required: true, message: 'Məbləğ daxil edilməyib!' }]
                })(
                  <Input type="number" style={{ width: '100%' }} min={0} placeholder="Ödəniləcək məbləğ..."/>
                )
              }
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="Ödəniş ediləcək son tarix">
              {
                getFieldDecorator('lastPaymentDate', {
                  rules: [{ required: true, message: 'Ödəniş ediləcək son tarix seçilməyib!' }]
                })(
                  <DatePicker placeholder="Son ödəniş tarixi..."/>
                )
              }
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item label="Dövri">
              {
                getFieldDecorator('isPeriodic', {
                  rules: [{ required: false }]
                })(
                  <Checkbox onChange={this.onPeriodicChanged}/>
                )
              }
            </Form.Item>
          </Col>
        </Row>
        {
          this.state.isPeriodic ? (
            <Row gutter={16}>
              <Col span={8}>
                <Form.Item label="Dövr">
                  {
                    getFieldDecorator('periodType', {
                      rules: [{ required: true, message: 'Dövr seçilməyib!' }]
                    })(
                      <Select placeholder="Ödəniş ediləcək dövrü seçin...">
                        {this.state.periodList.map(period => (
                          <Option key={period.value}>
                            {period.text}
                          </Option>
                        ))}
                      </Select>
                    )
                  }
                </Form.Item>
              </Col>
            </Row>
          ) : null
        }
      </React.Fragment>
    )
  }

}

TransferExpense.propTypes = {
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func
  }).isRequired
};

export default TransferExpense;