import React from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
  Form,
  Input,
  Checkbox,
  Tooltip,
  Icon,
  Select
} from 'antd';

const { Option } = Select;

class MobileCommunicationExpense extends React.Component {

  state = {
    mobileOperators: [
      { text: 'Azercell', value: 'azercell', prefixes: ['050', '051'] },
      { text: 'Bakcell', value: 'bakcell', prefixes: ['055'] },
      { text: 'Nar', value: 'nar', prefixes: [''] },
    ],
    periodList: [
      { text: 'Günlük', value: 'daily' },
      { text: 'Aylıq', value: 'monthly' },
      { text: 'illik', value: 'yearly' },
      { text: 'Rüblük', value: 'quarterly' }
    ],
    operatorPrefixes: [],
    isPeriodic: false
  };

  onOperatorChanged = (val) => {

    const prefixes = this.state.mobileOperators
      .filter(op => op.value === val)[0]['prefixes'];

    this.setState({
      operatorPrefixes: prefixes
    });
  };

  onPeriodicChanged = (e) => {
    this.setState({
      isPeriodic: e.target.checked
    });
  };

  render() {

    const { getFieldDecorator } = this.props.form;

    const operatorPrefix = (
      <Select value={this.state.operatorPrefixes[0]} style={{ width: 70 }}>
        {this.state.operatorPrefixes.map(prefix => (
          <Option key={prefix}>{prefix}</Option>
        ))}
      </Select>
    );

    return (
      <React.Fragment>
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item label="Ödənişin adı">
              {
                getFieldDecorator('title', {
                  rules: [{ required: true, message: 'Ödənişin adı daxil edilməyib!' }]
                })(
                  <Input placeholder="Ödənişin adını daxil edin..."/>
                )
              }
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="Mobil operator">
              {
                getFieldDecorator('mobileOperator', {
                  rules: [{ required: true, message: 'Mobil operator seçilməyib!' }]
                })(
                  <Select placeholder="Mobil operatoru seçin..." onChange={this.onOperatorChanged}>
                    {this.state.mobileOperators.map(operator => (
                      <Option key={operator.value}>
                        {operator.text}
                      </Option>
                    ))}
                  </Select>
                )
              }
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="Mobil nömrə">
              {
                getFieldDecorator('mobileNumber', {
                  rules: [{
                    required: true,
                    message: 'Mobil nömrə düzgün daxil edilməib!',
                    max: 7,
                    min: 7
                  }]
                })(
                  <Input type="number" addonBefore={operatorPrefix} placeholder="Mobil nömrə..."/>
                )
              }
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={8}>
            <Form.Item label={
              <span>
                Məbləğ (AZN) &nbsp;
                <Tooltip title="Ödənişin valyutası sizin hazırki valyuta seçiminizə görə müəyyən edilir">
                <Icon type="question-circle-o"/>
              </Tooltip>
            </span>
            }>
              {
                getFieldDecorator('amount', {
                  rules: [{ required: true, message: 'Məbləğ daxil edilməyib!' }]
                })(
                  <Input type="number" style={{ width: '100%' }} min={0} placeholder="Ödəniləcək məbləğ..."/>
                )
              }
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item label="Dövri">
              {
                getFieldDecorator('isPeriodic', {
                  rules: [{ required: false }]
                })(
                  <Checkbox onChange={this.onPeriodicChanged}/>
                )
              }
            </Form.Item>
          </Col>
          {
            this.state.isPeriodic ? (
              <Col span={8}>
                <Form.Item label="Dövr">
                  {
                    getFieldDecorator('periodType', {
                      rules: [{ required: true, message: 'Dövr seçilməyib!' }]
                    })(
                      <Select placeholder="Ödəniş ediləcək dövrü seçin...">
                        {this.state.periodList.map(period => (
                          <Option key={period.value}>
                            {period.text}
                          </Option>
                        ))}
                      </Select>
                    )
                  }
                </Form.Item>
              </Col>
            ) : null
          }
        </Row>
      </React.Fragment>
    );
  }

}

MobileCommunicationExpense.propTypes = {
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func
  }).isRequired
};

export default MobileCommunicationExpense;