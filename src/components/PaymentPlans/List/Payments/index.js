import React from 'react';
import { Table, Divider, message } from 'antd';
import { service } from '../../../../api/utils';
import moment from 'moment';

const { Column } = Table;

export default class PaymentList extends React.Component {

  state = {
    expenses: [],
    loading: false
  };

  componentDidMount() {
    this.setState({ loading: true });
    service.get('invoices')
      .then(response => {
        if (response.data) {
          this.setState({ expenses: response.data, loading: false });
        }
      }).catch(error => {
        message.error(error.message)
    });
  }

  render() {
    return (
      <React.Fragment>
        <h2 className="text-gradient">Ödənilməli</h2>
        <Table dataSource={this.state.expenses} loading={this.state.loading} rowKey={record => record.id}>
          <Column
            title="Ödənişin adı"
            dataIndex="name"
            key="name"
          />
          <Column
            title="Təsvir"
            dataIndex="description"
            key="description"
          />
          <Column
            title="Alıcı"
            dataIndex="recipient_name"
            key="recipient_name"
          />
          <Column
            title="Məbləğ"
            dataIndex="amount"
            key="amount"
          />
          <Column
            title="Son tarix"
            dataIndex="last_date"
            key="last_date"
            render={last_date => moment(last_date).format('LLL')}
          />
          <Column
            title=""
            key="id"
            render={(text, record) => (
              <span>
                <a href="#">Sil</a>
              </span>
            )}
          />
        </Table>
      </React.Fragment>
    );
  }
}