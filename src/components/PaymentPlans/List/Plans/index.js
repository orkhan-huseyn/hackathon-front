import React from 'react';
import { Table, Divider, Tag } from 'antd';
import { service } from '../../../../api/utils';

const { Column } = Table;

export default class PlanList extends React.Component {

  state = {
    data: [],
    loading: false
  };

  componentDidMount() {
    this.setState({ loading: true });
    service.get('templates')
      .then(response => {
        this.setState({ loading: false });

        if (response.data) {
          this.setState({data: response.data})
        }
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  }

  render() {
    return (
      <React.Fragment>
        <h2 className="text-gradient">Planlarım</h2>
        <Table dataSource={this.state.data} loading={this.state.loading} rowKey={record => record.id}>
          <Column
            title="Adı"
            dataIndex="name"
            key="name"
          />
          <Column
            title="Məbləğ"
            dataIndex="amount"
            key="amount"
          />
          <Column
            title="Period"
            dataIndex="period_name"
            key="period_name"
          />
          <Column
            title="Alıcı"
            dataIndex="recipient_name"
            key="recipient_name"
          />
          <Column
            title=""
            key="id"
            render={(text, record) => (
              <span>
                <a href="#">Sil</a>
              </span>
            )}
          />
        </Table>
      </React.Fragment>
    );
  }
}