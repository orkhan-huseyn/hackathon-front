import React from 'react';
import { Tabs } from 'antd';
import Payments from './Payments';
import Plans from './Plans';
import History from './History';

const TabPane = Tabs.TabPane;

export default class extends React.Component {

  render() {
    return (
      <Tabs type="card">
        <TabPane tab="Ödəniş planlarım" key="plans">
          <Plans/>
        </TabPane>
        <TabPane tab="Ödənilməli" key="payments">
          <Payments/>
        </TabPane>
        <TabPane tab="Tarixçə" key="history">
          <History/>
        </TabPane>
      </Tabs>
    );
  }

}