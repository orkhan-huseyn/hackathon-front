import React from 'react';
import { Table, Divider, Tag, message } from 'antd';
import { service } from "../../../../api/utils";
import moment from 'moment';

const { Column } = Table;

export default class History extends React.Component {

  state = {
    data: [],
    loading: false
  };

  componentDidMount() {
    this.getData()
  }

  getData = () => {
    this.setState({loading: true});
    service.get('transactions')
      .then(response => {
        if (response.data){
          this.setState({
            data: response.data,
            loading: false
          })
        }else{
          message.error('Xəta baş verdi')
        }
      })
      .catch(error => {
        message.error(error.message)
      })
  };

  render() {
    return (
      <React.Fragment>
        <h2 className="text-gradient">Tarixçə</h2>
        <Table dataSource={this.state.data} loading={this.state.loading} rowKey={record => record.id}>
          <Column
            title="Adı"
            dataIndex="name"
            key="name"
          />
          <Column
            title="Dövr"
            dataIndex="description"
            key="description"
          />
          <Column
            title="Dəyər"
            dataIndex="amount"
            key="amount"
          />
          <Column
            title="Tarix"
            dataIndex="transaction_date"
            key="transaction_date"
            render={transaction_date =>
              moment(transaction_date).format('LLL')
            }
          />
          <Column
            title=""
            key="id"
            render={(text, record) => (
              <span>
                <a href="#">Sil</a>
              </span>
            )}
          />
        </Table>
      </React.Fragment>
    );
  }
}

