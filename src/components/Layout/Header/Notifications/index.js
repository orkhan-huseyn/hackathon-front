import React from 'react';
import { NavLink } from 'react-router-dom';
import { List, Icon } from 'antd';

const data = [
  'Racing car sprays burning fuel into crowd.',
  'Japanese princess to wed commoner.',
  'Australian walks 100km after outback crash.',
  'Man charged over missing wedding girl.',
  'Los Angeles battles huge wildfires.',
];

export const notificationsContent = (
  <List
    size="small"
    footer={
      <NavLink to="/notifications">
        Hamsına bax &nbsp;
        <Icon type="arrow-right"/>
      </NavLink>
    }
    bordered
    dataSource={data}
    renderItem={item => (<List.Item>{item}</List.Item>)}
  />
);