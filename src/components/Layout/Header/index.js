import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Layout, Icon, Menu, Dropdown, Badge, Divider } from 'antd';
import { notificationsContent } from './Notifications';
import { USER_DISPLAY_NAME_KEY } from '../../../api/utils';

import './Header.scss'

const { Header } = Layout;

const userDropdownContent = (
  <Menu>
    <Menu.Item key="1">
      <NavLink to="/investment">
        <Icon type="dollar"/>
        &nbsp;İnvestisiya
      </NavLink>
    </Menu.Item>
    <Menu.Divider/>
    <Menu.Item key="3">
      <NavLink to="/logout">
        <Icon type="logout"/>
        &nbsp;Çıxış
      </NavLink>
    </Menu.Item>
  </Menu>
);

class AppHeader extends React.Component {

  render() {
    return (
      <Header className="Header">
        <Icon
          className="trigger"
          type={this.props.collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={this.props.toggle}
        />
        <span className="float-right right-side">
          <Dropdown overlay={notificationsContent} trigger={["click"]}>
            <Badge dot count={1}>
              <Icon type="bell" className="notification-icon"/>
            </Badge>
          </Dropdown>
          <Divider type="vertical"/>
          <Dropdown overlay={userDropdownContent} trigger={["click"]}>
            <span className="user-dropdown">
              {localStorage.getItem(USER_DISPLAY_NAME_KEY)} <Icon type="down" className="user-dropdown-icon"/>
            </span>
          </Dropdown>
        </span>
      </Header>
    );
  }
}

AppHeader.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired
};

export default AppHeader;
