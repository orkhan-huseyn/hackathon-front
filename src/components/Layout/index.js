import React from 'react';
import { Layout } from 'antd';
import Sidebar from './Sidebar';
import Header from "./Header";
import './Layout.scss';

const { Content, Footer } = Layout;

class AppLayout extends React.Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <Layout className="App">
        <Sidebar collapsed={this.state.collapsed}/>
        <Layout>
          <Header collapsed={this.state.collapsed} toggle={this.toggle}/>
          <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>
            {this.props.children}
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Made with <span className="heart">❤</span> by<a href="/"><span>&nbsp;Digital Team</span></a>
          </Footer>
        </Layout>
      </Layout>
    );
  }
}

export default AppLayout;
