import React from 'react';
import PropTypes from 'prop-types';
import { NavLink, withRouter } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';

import './Sidebar.scss';

const { Sider } = Layout;

const navList = [
  { path: '/', icon: 'appstore', title: 'Lövhə' },
  { path: '/payment-plans', icon: 'dollar', title: 'Ödənişlər planı' },
  { path: '/direct-pay', icon: 'bank', title: 'Birbaşa ödə' },
  { path: '/people', icon: 'team', title: 'Şəxslər' },
  { path: '/settings', icon: 'setting', title: 'Parametrlər' },
];

class Sidebar extends React.Component {

  state = {
    activePath: null
  };

  componentWillMount() {
    this.setState({
      activePath: this.props.location.pathname
    });
  }

  render() {

    return (
      <Sider
        trigger={null}
        collapsible
        collapsed={this.props.collapsed}
        className="Sider"
      >
        <div className="logo">Wallet</div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={[this.state.activePath]}>
          {
            navList.map(nav => (
              <Menu.Item key={nav.path}>
                <NavLink to={nav.path}>
                  <Icon type={nav.icon}/>
                  <span>{nav.title}</span>
                </NavLink>
              </Menu.Item>
            ))
          }
        </Menu>
      </Sider>
    );
  }

}

Sidebar.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};


export default withRouter(Sidebar);