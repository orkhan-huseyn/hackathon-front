import React from 'react';
import './BankCard.scss'

const cardLogo = "http://cmg.design/wp-content/uploads/2017/03/client-jpl-logo-wh.png";

export default class BankCard extends React.Component {
  render() {
    return (
      <div className="BankCard">
        <div className="card">
          <div className="card__front card__part">
            <img alt="..." className="card__front-square card__square" src={cardLogo} />
            <img alt="..." className="card__front-logo card__logo"
                 src="https://www.fireeye.com/partners/strategic-technology-partners/visa-fireeye-cyber-watch-program/_jcr_content/content-par/grid_20_80_full/grid-20-left/image.img.png/1505254557388.png"/>
            <p className="card_numer">{this.props.cardNo}</p>
            <div className="card__space-75">
              <span className="card__label">Card Holder</span>
              <p className="card__info">{this.props.cardHolder}</p>
            </div>
            <div className="card__space-25">
              <span className="card__label">Expires</span>
              <p className="card__info">{this.props.expires}</p>
            </div>
          </div>

          <div className="card__back card__part">
            <div className="card__black-line"></div>
            <div className="card__back-content">
              <div className="card__secret">
                <p className="card__secret--last">{this.props.cardCVC}</p>
              </div>
              <img alt="..." className="card__back-square card__square" src={cardLogo}/>
              <img alt="..." className="card__back-logo card__logo"
                   src="https://www.fireeye.com/partners/strategic-technology-partners/visa-fireeye-cyber-watch-program/_jcr_content/content-par/grid_20_80_full/grid-20-left/image.img.png/1505254557388.png"/>

            </div>
          </div>

        </div>
      </div>
    );
  }
}
