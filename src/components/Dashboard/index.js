import React from 'react';
import { Table, message} from 'antd';
import BankCard from "./BankCard";
import { service } from "../../api/utils";

export default class Dashboard extends React.Component {
  constructor() {
    super();

    this.state = {
      cards: []
    }
  }

  componentDidMount() {
    this.getCards()
  }

  getCards = () => {
    service.get('cards')
      .then(response => {
        if (response.data){
          this.setState({
            cards: response.data
          })
        }else{
          message.error('Xəta baş verdi')
        }
      })
      .catch(error => {
        message.error(error.message)
      })
  };

  render() {
    const dataSource = [{
      key: '1',
      name: 'Mike',
      age: 32,
      address: '10 Downing Street'
    }, {
      key: '2',
      name: 'John',
      age: 42,
      address: '10 Downing Street'
    }];

    const columns = [{
      title: 'Adı',
      dataIndex: 'name',
      key: 'name',
    }, {
      title: 'Məbləğ',
      dataIndex: 'age',
      key: 'age',
    }, {
      title: 'Nəsə',
      dataIndex: 'address',
      key: 'address',
    }];

    return (
      <React.Fragment>
        <div style={{marginBottom: 24}}>
          <h1 className="text-gradient">Kartlarım</h1>
          {
            this.state.cards.map(item => {
              return <BankCard
                cardNo={item.number}
                cardHolder={localStorage.getItem('usr_dsply_namee')}
                expires={item.exp_month + '/' + item.exp_year}
                cardCVC="963"
              />
            })
          }
        </div>

        <div>
          <h1 className="text-gradient">Son əməliyyatlar</h1>
          <Table dataSource={dataSource} columns={columns} pagination={false}/>
        </div>
      </React.Fragment>
    );
  }
}
