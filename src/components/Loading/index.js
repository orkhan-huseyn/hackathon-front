import React from 'react';
import { Icon } from 'antd';

export default () => (
  <div className="arrange-center text-center">
    <Icon type="loading" className="font-size-36"/>
    <h2>Yüklənir...</h2>
  </div>
);