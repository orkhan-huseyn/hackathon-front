import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'antd';

import './ELogin.scss';

class ELogin extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit} className="login-form m-auto mt-25">
        <div style={{ marginBottom: '15px' }}>
          Elektron imza kartınız və kart oxuyucu qurğu (“card reader”) və ya flaş imza kartı kompüterinizə
          qoşulmuşdurmu?
        </div>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Daxil ol
        </Button>
      </Form>
    );
  }
}

ELogin.propTypes = {
  redirect: PropTypes.func.isRequired
};

const WrappedSimpleLogin = Form.create()(ELogin);

export default WrappedSimpleLogin;