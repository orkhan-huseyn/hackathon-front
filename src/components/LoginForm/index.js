import React from 'react';
import { withRouter } from 'react-router-dom';
import { Row, Col, Tabs } from 'antd';
import SimpleLogin from './SimpleLogin';
import AsanLogin from './AsanLogin';
import ELogin from './ELogin';

import './LoginForm.scss';

const { TabPane } = Tabs;

class LoginForm extends React.Component {

  redirectToDashboard = () => {
    this.props.history.push('/');
  };

  render() {
    return (
      <Row className="LoginForm">
        <Col span={12} className="page-left">

        </Col>
        <Col span={12}>
          <div className="page-right w-100">
            <div className="text-center font-weight-300 mt-50">
              <h1>Hesabınıza daxil olun</h1>
              <p>Hesabınıza daxil olmaq üçün aşağıdakı metodlardan birini seçin</p>
            </div>
            <div>
              <Tabs type="card" className="m-auto">
                <TabPane tab="FİN ilə" key="1">
                  <SimpleLogin redirect={this.redirectToDashboard}/>
                </TabPane>
                <TabPane tab="ASAN İmza ilə" key="2">
                  <AsanLogin redirect={this.redirectToDashboard}/>
                </TabPane>
                <TabPane tab="Elektron İmza ilə" key="3">
                  <ELogin redirect={this.redirectToDashboard}/>
                </TabPane>
              </Tabs>
            </div>
          </div>
        </Col>
      </Row>
    );
  }

}

export default withRouter(LoginForm);