import React from 'react';
import PropTypes from 'prop-types';
import jwt_decode from 'jwt-decode';
import { Form, Icon, Input, Button, Checkbox, message } from 'antd';
import { service, TOKEN_KEY, USER_DISPLAY_NAME_KEY, USER_ID_KEY } from '../../../api/utils';

import './SimpleLogin.scss';

const FormItem = Form.Item;

class SimpleLogin extends React.Component {

  state = {
    loading: false
  };

  handleSubmit = (e) => {

    e.preventDefault();

    this.setState({ loading: true });

    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.checkLogin(values.pin, values.password);
      }
    });
  };

  checkLogin = (pin, pass) => {
    service.post('/users/authentication', {
      pin: pin,
      password: pass
    })
      .then(response => {
        this.setState({ loading: false });
        const token = response.data.token;
        let decoded = jwt_decode(token);
        localStorage.setItem(TOKEN_KEY, token);
        console.log(decoded);
        localStorage.setItem(USER_DISPLAY_NAME_KEY, decoded['userData']['fullName']);
        localStorage.setItem(USER_ID_KEY, decoded['userData']['userId']);
        this.props.redirect();
      })
      .catch(error => {
        message.error(error.message);
        this.setState({ loading: false });
      });
  };

  render() {

    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} className="login-form m-auto mt-25">
        <FormItem>
          {getFieldDecorator('pin', {
            rules: [{ required: true, message: 'FİN daxil edilməyib!' }],
          })(
            <Input prefix={<Icon type="idcard" style={{ color: 'rgba(0,0,0,.25)' }}/>} placeholder="FİN"/>
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Şifrə daxil edilməyib!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }}/>} type="password"
                   placeholder="Şifrə"/>
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(
            <Checkbox>Məni xatırla</Checkbox>
          )}
          <a className="login-form-forgot" href="/">Şifrəmi unutmuşam</a>
          <Button
            loading={this.state.loading}
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Daxil ol
          </Button>
        </FormItem>
      </Form>
    );
  }
}

SimpleLogin.propTypes = {
  redirect: PropTypes.func.isRequired
};

const WrappedSimpleLogin = Form.create()(SimpleLogin);

export default WrappedSimpleLogin;