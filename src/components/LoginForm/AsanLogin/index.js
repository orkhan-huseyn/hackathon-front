import React from 'react';
import PropTypes from 'prop-types';
import { Form, Icon, Input, Button, Select } from 'antd';

import './AsanLogin.scss';

const FormItem = Form.Item;
const { Option } = Select;

class AsanLogin extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const operators = (
      <Select value="50" style={{ width: 60 }}>
        <Option key="50">50</Option>
        <Option key="51">51</Option>
        <Option key="55">55</Option>
        <Option key="70">70</Option>
        <Option key="77">77</Option>
      </Select>
    );

    return (
      <Form onSubmit={this.handleSubmit} className="login-form m-auto mt-25">
        <FormItem>
          {getFieldDecorator('mobileNumber', {
            rules: [{ required: true, message: 'Mobil nömrə daxil edilməyib!', min: 7, max: 7 }],
          })(
            <Input type="number" addonBefore={operators} placeholder="Mobil nömrə"/>
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('userId', {
            rules: [{ required: true, message: 'İstidfadəçi İD-si daxil edilməyib!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }}/>} type="password"
                   placeholder="Istifadəçi İD-si"/>
          )}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Daxil ol
          </Button>
        </FormItem>
      </Form>
    );
  }
}

AsanLogin.propTypes = {
  redirect: PropTypes.func.isRequired
};

const WrappedAsanLogin = Form.create()(AsanLogin);

export default WrappedAsanLogin;