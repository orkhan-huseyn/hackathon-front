import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.scss';

import Logout from './pages/Logout';
import NotFound from './components/NotFound';
import LoadableLogin from './pages/Login/loadableLogin';
import LoadablePeople from './pages/People/loadablePeople';
import LoadableDashboard from './pages/Dashboard/loadableDashboard';
import LoadablePaymentPlans from './pages/PaymentPlans/loadablePaymentPlans';
import LoadableDirectPayment from './pages/DirectPayment/loadableDirectPayment';

class App extends Component {

  render() {

    return (
      <Switch>
        <Route path="/login" component={LoadableLogin}/>
        <Route path="/logout" component={Logout}/>
        <Route path="/" exact component={LoadableDashboard}/>
        <Route path="/people" component={LoadablePeople}/>
        <Route path="/direct-pay" component={LoadableDirectPayment}/>
        <Route path="/payment-plans" component={LoadablePaymentPlans}/>
        <Route component={NotFound}/>
      </Switch>
    );
  }
}

export default App;
