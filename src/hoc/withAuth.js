import React from 'react';
import { Redirect } from 'react-router-dom';
import { TOKEN_KEY } from '../api/utils';

export default function withAuth(WrappedComponent) {

  return class extends React.Component {

    render() {

      if (!localStorage.getItem(TOKEN_KEY)) {
        return <Redirect to="/login"/>;
      }

      return <WrappedComponent/>;
    }
  }

};
